# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 17:34:31 2018
@title: IMDBSentimentAnalysis
@author: sneha
"""
## Import libraries

import pandas as pd
import glob
import os
import numpy as np
""" Data Directory
    It is arragned in two folders, test and train
    Inside each directory we have pos and negative reviews.
    We are going to load files rom both the dirctories in our environment
"""

## Reviews do not have labels, We will have to add pos and neg labels 

labels = {'pos':1, 'neg':0}

df = pd.DataFrame()
"""
## Test path

test_path = r'C:\Users\sneha\Desktop\CleantGit\IMDBSentimentAnalysis\aclImdb\test'

all_files = glob.glob(os.path.join(test_path,"pos","*.*"))

df_from_each_file=(pd.read_table(f) for f in all_files)

concatenated_df=pd.concat(df_from_each_file,ignore_index=True)

all_files = glob.glob(os.path.join(test_path,"neg","*.*"))

df_from_each_file = (pd.read_table(f) for f in all_files)

concatenated_df_neg = pd.concat(df_from_each_file,ignore_index=True)



## Train path

train_path = r'C:\Users\sneha\Desktop\CleantGit\IMDBSentimentAnalysis\aclImdb\train'

all_files = glob.glob(os.path.join(train_path,"pos","*.*"))

df_from_each_file=(pd.read_table(f) for f in all_files)

concatenated_df=pd.concat(df_from_each_file,ignore_index=True)

all_files = glob.glob(os.path.join(train_path,"neg","*.*"))

df_from_each_file = (pd.read_table(f) for f in all_files)

concatenated_df_neg = pd.concat(df_from_each_file,ignore_index=True)
"""

for s in ('test','train'):
    for i in ('pos','neg'):
        path = 'C:/Users/sneha/Desktop/CleantGit/IMDBSentimentAnalysis/aclImdb/%s/%s' % (s,i)
        for file in os.listdir(path):
            with open(os.path.join(path,file),'r', encoding='utf-8') as infile:
                txt = infile.read()
            df = df.append([[txt,labels[i]]],ignore_index=True)
            print(infile)

### Write csv so that we do not have to read the multiple files again
            
df.to_csv("IMDBSentimentAnalysis.csv")

df = pd.read_csv("IMDBSentimentAnalysis.csv")

###When we read the files, we read all positive sentiments reviews and then all negative sentiment reviews
### We need to shuffle it.

df=df.reindex(np.random.permutation(df.index))

df.head(3)

df.columns = ['review','sentiment']

### Cleaning the data 

"""
We will remobve all punctuation marks
"""

import re

def preprocessor(text):
    ## remove html tags
    text = re.sub('<[^>]*>','',text)
    text = re.sub('[\W]+', ' ', text.lower())
    return text

preprocessor(df.loc[0,'review'][-50:])

df['review'] = df['review'].apply(preprocessor)

## Bag of words

""" Transforming words into feature vectors"""
""" We need to convert test and words into numerical form before passing the data to machine learning algorithms
"""

""" Raw term frequencies (tf) : the number of times a term t occurs in a documnet d"""
""" Some words are very frequently occuring in both the classes"""
""" term frequency - inverse document frequency
(tf-idf) can be used to downweight as those frequently occurring words in the feature vectors 

tf-idf(t,d) = tf(t,d) * (idf(t,d)+1)

idf(t,d) = log((1+n)/(1+df(d,t)))
n = total number of documents
df(d,t) = number of documents d that contains term t
"""

from nltk.stem.porter import PorterStemmer

porter = PorterStemmer()

def tokenizer(text):
    return text.split()

def tokenizer_porter(text):
    return [porter.stem(word) for word in text.split()]

tokenizer_porter('I like running long long hours')

import nltk

nltk.download('stopwords')

from nltk.corpus import stopwords

stop = stopwords.words('english')

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(df['review'],df['sentiment'],test_size=0.5,random_state=42)


from sklearn.model_selection import GridSearchCV

from sklearn.pipeline import Pipeline

from sklearn.linear_model import LogisticRegression

from sklearn.feature_extraction.text import TfidfVectorizer

tfidf = TfidfVectorizer(strip_accents = None,
                        lowercase = False,
                        preprocessor = None)


param_grid = [{'vect__ngram_range':[(1,1)],
                                    'vect__stop_words':[stop,None],
                                    'vect__tokenizer':[tokenizer,
                                                       tokenizer_porter],
                                                       'clf__penalty':['l1','l2'],
                                                       'clf__C':[1.0,10.0,100.0]},
    {'vect__ngram_range':[(1,1)],
                                    'vect__stop_words':[stop,None],
                                    'vect__tokenizer':[tokenizer,
                                                       tokenizer_porter],
                                                       'vect__use_idf':[False],
                                                       'vect__norm':[None],
                                                       'clf__penalty':['l1','l2'],
                                                       'clf__C':[1.0,10.0,100.0]}]
                                    


lr_tfidf = Pipeline([('vect',tfidf),
                     ('clf',LogisticRegression(random_state=0))])

gs_lr_tfidf = GridSearchCV(lr_tfidf,param_grid,
                           scoring='accuracy',
                           cv=5, verbose=1,
                           n_jobs=-1)

gs_lr_tfidf.fit(X_train, y_train)